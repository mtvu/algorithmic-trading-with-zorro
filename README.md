# algorithmic-trading-with-zorro

Code from Algorithmic Trading with Zorro

## Environment
Results and output obtained from running these scripts with Zorro version 1.96 and the AssetsFix.csv file located in this repository.

## Course Formatting Standards
Normal paragraph are 12pt Georgia font
Syntax highlighting is standard C using the Classic theme, font Monaco. 
When new terms relevant to the course are introduced, they are bolded and italicised once only. Subsequent uses are in normal paragraph font. 
The names of programming languages and file paths is in Courier New. 
What code examples should go into the Git Lab repo? Anything that complies and runs. Examples that don't run can be syntax highlighted in place. 