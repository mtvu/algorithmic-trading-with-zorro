var hypotenuse(var side1, var side2)
{
    return sqrt(pow(side1, 2) + pow(side2,2));
}

function main()
{
    var a, b, c;
    
    a = 39.014; //first short side
    b = 67.82; //second short side

    c = hypotenuse(a, b); //calculate length of hypotenuse

    printf("\nLength of short side 1: %f", a);
    printf("\nLength of short side 2: %f", b);
    printf("\nLength of hypotenuse: %f", c);
}