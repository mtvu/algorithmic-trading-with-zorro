function run()
{
    StartDate = 20160101;
    EndDate = 20160331;
    BarPeriod = 1440;
    
    //price series
    vars Price = series(price());
    vars Highs = series(priceHigh());
    vars Lows = series(priceLow());
    
    //short term highs and lows
    var fHigh = FractalHigh(Highs, 7);
    var fLow = FractalLow(Lows, 7);
    static var fHighCurrent, fLowCurrent;
    if (fHigh != 0) fHighCurrent = fHigh;
    if (fLow !=0) fLowCurrent = fLow;
    
    plot("Recent High", fHighCurrent, MAIN|DOT, BLUE);
    plot("Recent Low", fLowCurrent, MAIN|DOT, RED);

}