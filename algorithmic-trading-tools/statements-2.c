//print the previous closing price:
printf("Yesterday's closing price: \%.5f", priceClose(0));

//enter a long trade of 5 lots if price reaches 10 points above the previous high:
enterLong(5, priceHigh(0) + 10*PIP);

//if the trade goes into profit, move the stop loss to the lowest low of the previous 5 periods, if that value exceeds the existing stop loss:
if (TradeProfit > 0) TradeStopLimit = max(TradeStopLimit, LL(5));