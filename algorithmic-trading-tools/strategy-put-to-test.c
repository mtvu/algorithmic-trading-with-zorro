function run() 
{
    // Set up simulation variables
    BarPeriod = 15;
    StartDate = 20061210;
    EndDate = 20171231;
    LookBack = 1500;
    asset("GBP/USD");
	
	// 4-hourly prices and indicators
    TimeFrame = 16;

    vars H4open = series(priceOpen());
	vars H4high = series(priceHigh());
	vars H4low = series(priceLow());
	vars H4close = series(priceClose());
	
    vars H4FilterFast = series(LowPass(H4close, 5));
    vars H4FilterSlow = series(LowPass(H4close, 10));
    vars H4rsi = series(RSI(H4close, 9));
    Stoch(H4open, H4high, H4low, H4close, 3, 10, MAType_EMA, 3, MAType_EMA);
    vars H4stochK = series(rSlowK);
    vars H4stochD = series(rSlowD);
	
	// 15-minute prices and indicators
    TimeFrame = 1;

    vars open = series(priceOpen());
	vars high = series(priceHigh());
	vars low = series(priceLow());
	vars close = series(priceClose());
	
    vars filterFast = series(LowPass(close, 5));
    vars filterSlow = series(LowPass(close, 10));
    vars rsi = series(RSI(close, 9));
    Stoch(open, high, low, close, 3, 10, MAType_EMA, 3, MAType_EMA);
    vars stochK = series(rSlowK);
    vars stochD = series(rSlowD);
    MACD(close, 12, 26, 9);
    vars macdHist = series(rMACDHist);

    var swingHigh = HH(16);
    var swingLow = LL(16);

    // Long trades
    if (H4FilterFast[0] > H4FilterSlow[0] and H4rsi[0] > 50 and H4stochK[0] > H4stochD[0]) 
    {
        if (NumOpenLong == 0 and crossOver(filterFast, filterSlow) and rsi[0] > 50 
        and stochK[0] > stochD[0] and ((rising(macdHist) and macdHist[0] < 0) 
        or crossOver(macdHist, 0)) ) 
        {
            Stop = swingLow;
            TakeProfit = close[0] - swingLow;
            enterLong();
        }
    }

    // Short trades
    if (H4FilterFast[0] < H4FilterSlow[0] and H4rsi[0] < 50 and H4stochK[0] < H4stochD[0]) 
    {
        if (NumOpenShort == 0 and crossUnder(filterFast, filterSlow) and rsi[0] < 50 
        and stochK[0] < stochD[0] and ((falling(macdHist) and macdHist[0] > 0) 
        or crossUnder(macdHist, 0)) ) 
        {
            Stop = swingHigh;
            TakeProfit = swingHigh - close[0];
            enterShort();
        }
    }
	
	// plotting 
	setf(PlotMode, PL_FINE);
	PlotWidth = 1400;
	PlotHeight1 = 300;
    ColorUp = ColorDn = ColorWin = ColorLoss = ColorDD = 0;
	ColorEquity = 0x00666699;
}