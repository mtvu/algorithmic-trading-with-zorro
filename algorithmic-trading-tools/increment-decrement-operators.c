void main()
{
	int x, y, z;
	
	x = 10; // x’s starting value is 10
	y = x++; // y is now 10, x is now 11
	z = ++x; // z and x are now both 12
	y = --x; // y and x are now both 11
	z = x--; // z is now 11, x is now 10
	
	printf("\nx: %d, y: %d, z: %d", x, y, z);
	
}