function run()
{
	LookBack = 0; //start trading immediately
	
	Entry = 100*PIP; //this Entry parameters will be overridden
	
	enterLong(50*PIP, 25*PIP, 250*PIP); //enter pending buy stop, override global entry/exit parameters
	enterLong(-50*PIP, 25*PIP, 250*PIP); //enter pending buy limit, override global entry/exit parameters
	
	if(Bar > 5)
		quit(); // quit after five bars
}