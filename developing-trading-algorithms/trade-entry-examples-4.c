function run()
{
	LookBack = 0; //start trading immediately

	Entry = 50*PIP; //pending stop entry distance
	Stop = 25*PIP; //initial stop loss
	Trail = 40*PIP; //trail the maximum profit by 65 pips
	TrailSlope = 50; //raise the stop loss by 50\% of maximum favorable excursion once trade goes into profit by Trail pips
	TakeProfit = 250*PIP; //take profit target
	
	enterLong(); //enter a pending buy stop
	
	if(Bar > 5)
		quit(); // quit after five bars
}