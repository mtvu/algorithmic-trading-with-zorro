function run()
{
	LookBack = 0; //start trading immediately
	
	Entry = 50*PIP; //pending stop order
	Stop = 25*PIP; //initial stop loss
	Trail = 40*PIP; //trail the maximum profit by 65 pips
	TrailLock = 1; //lock 1\% of profit once trade profit exceeds Trail distance
	TakeProfit = 250*PIP; //take profit target
	
	enterLong(); //enter a pending buy stop order
	
	if(Bar > 5)
		quit(); // quit after five bars
	
}