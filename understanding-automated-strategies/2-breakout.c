/* 
Donchian Channel Breakout Strategy

*/

function run()
{
	set(PLOTNOW);
	setf(PlotMode, PL_FINE);
	StartDate = 20100101;
	EndDate = 20181231;
	MaxLong = MaxShort = 1;
	BarPeriod = 1440;
	
	int donchPeriod = 20;
	DChannel(donchPeriod);
	
	Stop = 1.5*ATR(25);
	Trail = 0.5*Stop;
	
	if(priceClose() < rRealUpperBand and NumOpenShort + NumOpenLong == 0)
	{
		Entry = rRealUpperBand;
		enterLong();
	}
	
	if(priceClose() > rRealLowerBand and NumOpenShort + NumOpenLong == 0)
	{
		Entry = rRealLowerBand;
		enterShort();
	}

	plot("upperDchannel", rRealUpperBand, BAND1, 0x00CC00);
	plot("lowerDchannel", rRealLowerBand, BAND2, 0xCC00FF00);
}